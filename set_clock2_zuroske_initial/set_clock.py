################################################################################
# set_clock2_zuroske_initial.py
# T. Zuroske - Sept 2016
#
# A program that allows a user to set a clock using only auditory commands.
#
# Some sample code used from sample_menu.py implemented by A. Hornof - Sept 2016
################################################################################

__author__ = 'zuroske'

# Package imports
import readchar
# import day
from tkinter import *
from tkinter import messagebox

# Local imports
from sound import Sound
import day


FORWARD = 'j'
BACKWARD = 'k'
SELECT = ' '
LAST_DAY = 6
FIRST_DAY = 0
DAY_NUMBER = 0
DEFAULT_DAY = "Sunday"
DEFAULT_HOUR = 1
DEFAULT_MINUTE = 00
DEFAULT_AMPM = "AM"
SAVED_DAY = None
SAVED_HOUR = None
SAVED_MINUTE = None
SAVED_AMPM = None
SAVED_TIME = "SavedTime.txt"
DAYS = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
        "SATURDAY"]
EXITING_PROGRAM = Sound \
                        ("wav_files/menus_modes_navigation_m/"
                         "Exiting_program_m.wav")


class MyApp:
    def __init__(self, parent):
        parent.minsize(width=850, height=400)
        parent.maxsize(width=850, height=400)
        self.myParent = parent
        self.myContainer1 = Frame(parent)
        self.myContainer1.grid(row=0, column=0, sticky=W)

        self.menu_text = Label(self.myContainer1, text="Menu", font="Times 16 bold")
        self.menu_text2 = Label(self.myContainer1, text="Menu Items",
                                font="Times 16 bold")
        self.menu_text.grid(row=0, column=0, sticky=W)
        self.menu_text2.grid(row=0, column=1, sticky=W, padx=(50, 0))

        self.mainMenu = Label(self.myContainer1)
        self.mainMenu.configure(text="Main Menu", anchor=W,
                                background='red', justify=LEFT, height=1, width=10)
        self.mainMenu.grid(row=1, column=0, sticky=W)

        self.menuSetDay = Button(self.myContainer1)
        self.menuSetDay.configure(text="Set Day", highlightbackground='red',
                                  justify=LEFT, height=1, width=10)
        self.menuSetDay.bind("<space>", self.setDayClick)
        self.menuSetDay.bind("<j>", lambda event, obj="setDay":
                             self.moveMenuSpotLeft(event, obj))
        self.menuSetDay.bind("<k>", lambda event, obj="setDay":
                             self.moveMenuSpotRight(event, obj))

        self.menuSetDay.grid(row=1, column=1, sticky=W, padx=(50, 0))
        self.menuSetDay.focus_force()

        self.menuSetHour = Button(self.myContainer1)
        self.menuSetHour.configure(text="Set Hour", background="red",
                                  justify=LEFT, height=1, width=10)
        self.menuSetHour.grid(row=1, column=2, sticky=W)

        self.menuSetHour.bind("<space>", self.setHourClick)
        self.menuSetHour.bind("<j>", lambda event, obj="setHour":
                              self.moveMenuSpotLeft(event, obj))
        self.menuSetHour.bind("<k>", lambda event, obj="setHour":
                             self.moveMenuSpotRight(event, obj))

        self.menuSetMinute = Button(self.myContainer1)
        self.menuSetMinute.configure(text="Set Minute", background="red",
                                   justify=LEFT, height=1, width=10)
        self.menuSetMinute.grid(row=1, column=3, sticky=W)
        self.menuSetMinute.bind("<space>", self.setMinuteClick)
        self.menuSetMinute.bind("<j>", lambda event, obj="setMin":
                                self.moveMenuSpotLeft(event, obj))
        self.menuSetMinute.bind("<k>", lambda event, obj="setMin":
                                self.moveMenuSpotRight(event, obj))

        self.menuExit = Button(self.myContainer1)
        self.menuExit.configure(text="Exit Program", background="red",
                                     justify=LEFT, height=1, width=10)
        self.menuExit.grid(row=1, column=4, sticky=W)

        self.menuExit.bind("<j>", lambda event, obj="exitProgram":
                                self.moveMenuSpotLeft(event, obj))
        self.menuExit.bind("<k>", lambda event, obj="exitProgram":
                             self.moveMenuSpotRight(event, obj))
        self.menuExit.bind("<space>", self.exitProgramPress)

        self.imagePath = "images/instructions.gif"
        self.image = PhotoImage(file=self.imagePath)
        self.displayInstructImage = self.image.subsample(2, 2)
        self.imageLabel = Label(self.myParent, image=self.displayInstructImage)
        self.imageLabel.place(relx=1.0, rely=1.0, anchor="se")
        if (currentClock.getCurrentMinute() < 10):
            self.minClockDisplace = "0" + str(currentClock.getCurrentMinute())
        else:
            self.minClockDisplace = str(currentClock.getCurrentMinute())
        self.currentTimeLabel = Label(self.myParent,
                                      text="Current Set Time: " +
                            str(currentClock.getCurrentDay()) + " " +
                            str(currentClock.getCurrentHour())+ ":" +
                            self.minClockDisplace + " " +
                            str(currentClock.getCurrentAMPM()),
                            font="Times 30 bold")
        self.currentTimeLabel.place(relx=0.0, rely=0.5, anchor="w")

        self.myParent.update()

        mainMenuMode = Sound \
            ("wav_files/custom_sounds/Main_Menu_Mode.wav")
        mainMenuMode.play_to_end()
        clockSound = Sound \
            ("wav_files/custom_sounds/Clock_currently_set_to.wav")
        clockSound.play_to_end()
        currentClock.playCurrentDayAndTime()

        self.myContainer2 = Frame(self.myParent)
        self.myContainer2.grid(row=2, column=0, sticky=W)

        self.setDay = Label(self.myContainer2)
        self.setDay.configure(text="Set Day", justify=LEFT, anchor=W,
                         background='red', height=1, width=10)
        self.setDay.grid(row=0, column=0, sticky=W)

        self.sunday = Button(self.myContainer2)
        self.sunday.configure(text="Sunday", justify=LEFT, height=1,
                         width=10)
        self.sunday.grid(row=0, column=1, sticky=W, padx=(50, 0))

        self.monday = Button(self.myContainer2)
        self.monday.configure(text="Monday", justify=LEFT, height=1, width=10)
        self.monday.grid(row=0, column=2, sticky=W)

        self.tuesday = Button(self.myContainer2)
        self.tuesday.configure(text="Tuesday", justify=LEFT, height=1, width=10)
        self.tuesday.grid(row=0, column=3, sticky=W)

        self.wednesday = Button(self.myContainer2)
        self.wednesday.configure(text="Wednesday", justify=LEFT, height=1, width=10)
        self.wednesday.grid(row=0, column=4, sticky=W)

        self.thursday = Button(self.myContainer2)
        self.thursday.configure(text="Thursday", justify=LEFT, height=1, width=10)
        self.thursday.grid(row=0, column=5, sticky=W)

        self.friday = Button(self.myContainer2)
        self.friday.configure(text="Friday", justify=LEFT, height=1, width=10)
        self.friday.grid(row=0, column=6, sticky=W)

        self.saturday = Button(self.myContainer2)
        self.saturday.configure(text="Saturday", justify=LEFT, height=1, width=10)
        self.saturday.grid(row=0, column=7, sticky=W)

        self.sunday.bind("<k>", lambda event, obj="sunday":
                                self.moveMenuSpotRight(event, obj))
        self.monday.bind("<k>", lambda event, obj="monday":
                                self.moveMenuSpotRight(event, obj))
        self.tuesday.bind("<k>", lambda event, obj="tuesday":
                                self.moveMenuSpotRight(event, obj))
        self.wednesday.bind("<k>", lambda event, obj="wednesday":
                                self.moveMenuSpotRight(event, obj))
        self.thursday.bind("<k>", lambda event, obj="thursday":
                                self.moveMenuSpotRight(event, obj))
        self.friday.bind("<k>", lambda event, obj="friday":
                                self.moveMenuSpotRight(event, obj))
        self.saturday.bind("<k>", lambda event, obj="saturday":
                         self.moveMenuSpotRight(event, obj))


        self.sunday.bind("<j>", lambda event, obj="sunday":
                                self.moveMenuSpotLeft(event, obj))
        self.monday.bind("<j>", lambda event, obj="monday":
                                self.moveMenuSpotLeft(event, obj))
        self.tuesday.bind("<j>", lambda event, obj="tuesday":
                                 self.moveMenuSpotLeft(event, obj))
        self.wednesday.bind("<j>", lambda event, obj="wednesday":
                                  self.moveMenuSpotLeft(event, obj))
        self.thursday.bind("<j>", lambda event, obj="thursday":
                                self.moveMenuSpotLeft(event, obj))
        self.friday.bind("<j>", lambda event, obj="friday":
                                self.moveMenuSpotLeft(event, obj))
        self.saturday.bind("<j>", lambda event, obj="saturday":
                                self.moveMenuSpotLeft(event, obj))

        self.sunday.bind("<space>", lambda event, obj="sunday":
                            self.setDaySelect(event, obj))
        self.monday.bind("<space>", lambda event, obj="monday":
                            self.setDaySelect(event, obj))
        self.tuesday.bind("<space>", lambda event, obj="tuesday":
                            self.setDaySelect(event, obj))
        self.wednesday.bind("<space>", lambda event, obj="wednesday":
                            self.setDaySelect(event, obj))
        self.thursday.bind("<space>", lambda event, obj="thursday":
                            self.setDaySelect(event, obj))
        self.friday.bind("<space>", lambda event, obj="friday":
                            self.setDaySelect(event, obj))
        self.saturday.bind("<space>", lambda event, obj="saturday":
                            self.setDaySelect(event, obj))

        self.myContainer2.grid_forget()

        #Create hour widgets
        self.myContainer3 = Frame(self.myParent)
        self.myContainer3.grid(row=2, column=0, sticky=W)

        self.setHour = Label(self.myContainer3)
        self.setHour.configure(text="Set Hour", justify=LEFT, anchor=W,
                          background='red', height=1, width=10)
        self.setHour.grid(row=0, column=0, sticky=W)

        self.oneAM = Button(self.myContainer3)
        self.oneAM.configure(text="01 AM", anchor=W, justify=LEFT, height=1,
                      width=10)
        self.oneAM.grid(row=0, column=1, sticky=W, padx=(50, 0))

        self.twoAM = Button(self.myContainer3)
        self.twoAM.configure(text="02 AM", justify=LEFT, height=1, width=10)
        self.twoAM.grid(row=0, column=2, sticky=W)

        self.threeAM = Button(self.myContainer3)
        self.threeAM.configure(text="03 AM", justify=LEFT, height=1, width=10)
        self.threeAM.grid(row=0, column=3, sticky=W)

        self.fourAM = Button(self.myContainer3)
        self.fourAM.configure(text="04 AM", justify=LEFT, height=1, width=10)
        self.fourAM.grid(row=0, column=4, sticky=W)

        self.fiveAM = Button(self.myContainer3)
        self.fiveAM.configure(text="05 AM", justify=LEFT, height=1, width=10)
        self.fiveAM.grid(row=0, column=5, sticky=W)

        self.sixAM = Button(self.myContainer3)
        self.sixAM.configure(text="06 AM", justify=LEFT, height=1, width=10)
        self.sixAM.grid(row=0, column=6, sticky=W)

        self.sevenAM = Button(self.myContainer3)
        self.sevenAM.configure(text="07 AM", justify=LEFT, height=1, width=10)
        self.sevenAM.grid(row=1, column=1, sticky=W, padx=(50, 0))

        self.eightAM = Button(self.myContainer3)
        self.eightAM.configure(text="08 AM", justify=LEFT, height=1,
                        width=10)
        self.eightAM.grid(row=1, column=2, sticky=W)

        self.nineAM = Button(self.myContainer3)
        self.nineAM.configure(text="09 AM", justify=LEFT, height=1, width=10)
        self.nineAM.grid(row=1, column=3, sticky=W)

        self.tenAM = Button(self.myContainer3)
        self.tenAM.configure(text="10 AM", justify=LEFT, height=1, width=10)
        self.tenAM.grid(row=1, column=4, sticky=W)

        self.elevenAM = Button(self.myContainer3)
        self.elevenAM.configure(text="11 AM", justify=LEFT, height=1, width=10)
        self.elevenAM.grid(row=1, column=5, sticky=W)

        self.twelveAM = Button(self.myContainer3)
        self.twelveAM.configure(text="12 AM", justify=LEFT, height=1, width=10)
        self.twelveAM.grid(row=1, column=6, sticky=W)

        self.onePM = Button(self.myContainer3)
        self.onePM.configure(text="01 PM", anchor=W, justify=LEFT, height=1,
                             width=10)
        self.onePM.grid(row=2, column=1, sticky=W, padx=(50, 0))

        self.twoPM = Button(self.myContainer3)
        self.twoPM.configure(text="02 PM", justify=LEFT, height=1, width=10)
        self.twoPM.grid(row=2, column=2, sticky=W)

        self.threePM = Button(self.myContainer3)
        self.threePM.configure(text="03 PM", justify=LEFT, height=1, width=10)
        self.threePM.grid(row=2, column=3, sticky=W)

        self.fourPM = Button(self.myContainer3)
        self.fourPM.configure(text="04 PM", justify=LEFT, height=1, width=10)
        self.fourPM.grid(row=2, column=4, sticky=W)

        self.fivePM = Button(self.myContainer3)
        self.fivePM.configure(text="05 PM", justify=LEFT, height=1, width=10)
        self.fivePM.grid(row=2, column=5, sticky=W)

        self.sixPM = Button(self.myContainer3)
        self.sixPM.configure(text="06 PM", justify=LEFT, height=1, width=10)
        self.sixPM.grid(row=2, column=6, sticky=W)

        self.sevenPM = Button(self.myContainer3)
        self.sevenPM.configure(text="07 PM", justify=LEFT, height=1, width=10)
        self.sevenPM.grid(row=3, column=1, sticky=W, padx=(50, 0))

        self.eightPM = Button(self.myContainer3)
        self.eightPM.configure(text="08 PM ", justify=LEFT, height=1,
                               width=10)
        self.eightPM.grid(row=3, column=2, sticky=W)

        self.ninePM = Button(self.myContainer3)
        self.ninePM.configure(text="09 PM", justify=LEFT, height=1, width=10)
        self.ninePM.grid(row=3, column=3, sticky=W)

        self.tenPM = Button(self.myContainer3)
        self.tenPM.configure(text="10 PM", justify=LEFT, height=1, width=10)
        self.tenPM.grid(row=3, column=4, sticky=W)

        self.elevenPM = Button(self.myContainer3)
        self.elevenPM.configure(text="11 PM", justify=LEFT, height=1, width=10)
        self.elevenPM.grid(row=3, column=5, sticky=W)

        self.twelvePM = Button(self.myContainer3)
        self.twelvePM.configure(text="12 PM", justify=LEFT, height=1, width=10)
        self.twelvePM.grid(row=3, column=6, sticky=W)

        self.myContainer3.grid_forget()

        self.oneAM.bind("<k>", lambda event, obj="oneAM":
                        self.moveMenuSpotRight(event, obj))
        self.onePM.bind("<k>", lambda event, obj="onePM":
                        self.moveMenuSpotRight(event, obj))
        self.twoAM.bind("<k>", lambda event, obj="twoAM":
                        self.moveMenuSpotRight(event, obj))
        self.twoPM.bind("<k>", lambda event, obj="twoPM":
                        self.moveMenuSpotRight(event, obj))
        self.threeAM.bind("<k>", lambda event, obj="threeAM":
                        self.moveMenuSpotRight(event, obj))
        self.threePM.bind("<k>", lambda event, obj="threePM":
                        self.moveMenuSpotRight(event, obj))
        self.fourAM.bind("<k>", lambda event, obj="fourAM":
                         self.moveMenuSpotRight(event, obj))
        self.fourPM.bind("<k>", lambda event, obj="fourPM":
                         self.moveMenuSpotRight(event, obj))
        self.fiveAM.bind("<k>", lambda event, obj="fiveAM":
                         self.moveMenuSpotRight(event, obj))
        self.fivePM.bind("<k>", lambda event, obj="fivePM":
                         self.moveMenuSpotRight(event, obj))
        self.sixAM.bind("<k>", lambda event, obj="sixAM":
                        self.moveMenuSpotRight(event, obj))
        self.sixPM.bind("<k>", lambda event, obj="sixPM":
                        self.moveMenuSpotRight(event, obj))
        self.sevenAM.bind("<k>", lambda event, obj="sevenAM":
                          self.moveMenuSpotRight(event, obj))
        self.sevenPM.bind("<k>", lambda event, obj="sevenPM":
                            self.moveMenuSpotRight(event, obj))
        self.eightAM.bind("<k>", lambda event, obj="eightAM":
                    self.moveMenuSpotRight(event, obj))
        self.eightPM.bind("<k>", lambda event, obj="eightPM":
                            self.moveMenuSpotRight(event, obj))
        self.nineAM.bind("<k>", lambda event, obj="nineAM":
                            self.moveMenuSpotRight(event, obj))
        self.ninePM.bind("<k>", lambda event, obj="ninePM":
                         self.moveMenuSpotRight(event, obj))
        self.tenAM.bind("<k>", lambda event, obj="tenAM":
                        self.moveMenuSpotRight(event, obj))
        self.tenPM.bind("<k>", lambda event, obj="tenPM":
                        self.moveMenuSpotRight(event, obj))
        self.elevenAM.bind("<k>", lambda event, obj="elevenAM":
                            self.moveMenuSpotRight(event, obj))
        self.elevenPM.bind("<k>", lambda event, obj="elevenPM":
                            self.moveMenuSpotRight(event, obj))
        self.twelveAM.bind("<k>", lambda event, obj="twelveAM":
                            self.moveMenuSpotRight(event, obj))
        self.twelvePM.bind("<k>", lambda event, obj="twelvePM":
                            self.moveMenuSpotRight(event, obj))

        self.oneAM.bind("<j>", lambda event, obj="oneAM":
                        self.moveMenuSpotLeft(event, obj))
        self.onePM.bind("<j>", lambda event, obj="onePM":
                        self.moveMenuSpotLeft(event, obj))
        self.twoAM.bind("<j>", lambda event, obj="twoAM":
                        self.moveMenuSpotLeft(event, obj))
        self.twoPM.bind("<j>", lambda event, obj="twoPM":
                        self.moveMenuSpotLeft(event, obj))
        self.threeAM.bind("<j>", lambda event, obj="threeAM":
                          self.moveMenuSpotLeft(event, obj))
        self.threePM.bind("<j>", lambda event, obj="threePM":
                          self.moveMenuSpotLeft(event, obj))
        self.fourAM.bind("<j>", lambda event, obj="fourAM":
                         self.moveMenuSpotLeft(event, obj))
        self.fourPM.bind("<j>", lambda event, obj="fourPM":
                         self.moveMenuSpotLeft(event, obj))
        self.fiveAM.bind("<j>", lambda event, obj="fiveAM":
                         self.moveMenuSpotLeft(event, obj))
        self.fivePM.bind("<j>", lambda event, obj="fivePM":
                         self.moveMenuSpotLeft(event, obj))
        self.sixAM.bind("<j>", lambda event, obj="sixAM":
                        self.moveMenuSpotLeft(event, obj))
        self.sixPM.bind("<j>", lambda event, obj="sixPM":
                        self.moveMenuSpotLeft(event, obj))
        self.sevenAM.bind("<j>", lambda event, obj="sevenAM":
                          self.moveMenuSpotLeft(event, obj))
        self.sevenPM.bind("<j>", lambda event, obj="sevenPM":
                          self.moveMenuSpotLeft(event, obj))
        self.eightAM.bind("<j>", lambda event, obj="eightAM":
                          self.moveMenuSpotLeft(event, obj))
        self.eightPM.bind("<j>", lambda event, obj="eightPM":
                          self.moveMenuSpotLeft(event, obj))
        self.nineAM.bind("<j>", lambda event, obj="nineAM":
                         self.moveMenuSpotLeft(event, obj))
        self.ninePM.bind("<j>", lambda event, obj="ninePM":
                         self.moveMenuSpotLeft(event, obj))
        self.tenAM.bind("<j>", lambda event, obj="tenAM":
                        self.moveMenuSpotLeft(event, obj))
        self.tenPM.bind("<j>", lambda event, obj="tenPM":
                        self.moveMenuSpotLeft(event, obj))
        self.elevenAM.bind("<j>", lambda event, obj="elevenAM":
                           self.moveMenuSpotLeft(event, obj))
        self.elevenPM.bind("<j>", lambda event, obj="elevenPM":
                           self.moveMenuSpotLeft(event, obj))
        self.twelveAM.bind("<j>", lambda event, obj="twelveAM":
                           self.moveMenuSpotLeft(event, obj))
        self.twelvePM.bind("<j>", lambda event, obj="twelvePM":
                           self.moveMenuSpotLeft(event, obj))

        self.oneAM.bind("<space>", lambda event, obj="oneAM":
                        self.setHourSelect(event, obj))
        self.onePM.bind("<space>", lambda event, obj="onePM":
                        self.setHourSelect(event, obj))
        self.twoAM.bind("<space>", lambda event, obj="twoAM":
                        self.setHourSelect(event, obj))
        self.twoPM.bind("<space>", lambda event, obj="twoPM":
                        self.setHourSelect(event, obj))
        self.threeAM.bind("<space>", lambda event, obj="threeAM":
                          self.setHourSelect(event, obj))
        self.threePM.bind("<space>", lambda event, obj="threePM":
                          self.setHourSelect(event, obj))
        self.fourAM.bind("<space>", lambda event, obj="fourAM":
                            self.setHourSelect(event, obj))
        self.fourPM.bind("<space>", lambda event, obj="fourPM":
                            self.setHourSelect(event, obj))
        self.fiveAM.bind("<space>", lambda event, obj="fiveAM":
                            self.setHourSelect(event, obj))
        self.fivePM.bind("<space>", lambda event, obj="fivePM":
                            self.setHourSelect(event, obj))
        self.sixAM.bind("<space>", lambda event, obj="sixAM":
                            self.setHourSelect(event, obj))
        self.sixPM.bind("<space>", lambda event, obj="sixPM":
                            self.setHourSelect(event, obj))
        self.sevenAM.bind("<space>", lambda event, obj="sevenAM":
                            self.setHourSelect(event, obj))
        self.sevenPM.bind("<space>", lambda event, obj="sevenPM":
                            self.setHourSelect(event, obj))
        self.eightAM.bind("<space>", lambda event, obj="eightAM":
                            self.setHourSelect(event, obj))
        self.eightPM.bind("<space>", lambda event, obj="eightPM":
                            self.setHourSelect(event, obj))
        self.nineAM.bind("<space>", lambda event, obj="nineAM":
                            self.setHourSelect(event, obj))
        self.ninePM.bind("<space>", lambda event, obj="ninePM":
                            self.setHourSelect(event, obj))
        self.tenAM.bind("<space>", lambda event, obj="tenAM":
                            self.setHourSelect(event, obj))
        self.tenPM.bind("<space>", lambda event, obj="tenPM":
                            self.setHourSelect(event, obj))
        self.elevenAM.bind("<space>", lambda event, obj="elevenAM":
                            self.setHourSelect(event, obj))
        self.elevenPM.bind("<space>", lambda event, obj="elevenPM":
                            self.setHourSelect(event, obj))
        self.twelveAM.bind("<space>", lambda event, obj="twelveAM":
                            self.setHourSelect(event, obj))
        self.twelvePM.bind("<space>", lambda event, obj="twelvePM":
                            self.setHourSelect(event, obj))

        #Create minute widgets

        self.myContainer4 = Frame(self.myParent)
        self.myContainer4.grid(row=2, column=0, sticky=W)

        self.setMin = Label(self.myContainer4)
        self.setMin.configure(text="Set Minute", justify=LEFT, anchor=W,
                         background='red', height=1, width=10)
        self.setMin.grid(row=0, column=0, sticky=W)

        self.zero = Button(self.myContainer4)
        self.zero.configure(text="00", anchor=W, justify=LEFT, height=1,
                       width=10)
        self.zero.grid(row=0, column=1, sticky=W, padx=(50, 0))

        self.five = Button(self.myContainer4)
        self.five.configure(text="05", justify=LEFT, height=1, width=10)
        self.five.grid(row=0, column=2, sticky=W)

        self.ten = Button(self.myContainer4)
        self.ten.configure(text="10", justify=LEFT, height=1, width=10)
        self.ten.grid(row=0, column=3, sticky=W)

        self.fifteen = Button(self.myContainer4)
        self.fifteen.configure(text="15", justify=LEFT, height=1, width=10)
        self.fifteen.grid(row=0, column=4, sticky=W)

        self.twenty = Button(self.myContainer4)
        self.twenty.configure(text="20", justify=LEFT, height=1, width=10)
        self.twenty.grid(row=0, column=5, sticky=W)

        self.twentyfive = Button(self.myContainer4)
        self.twentyfive.configure(text="25", justify=LEFT, height=1, width=10)
        self.twentyfive.grid(row=0, column=6, sticky=W)

        self.thirty = Button(self.myContainer4)
        self.thirty.configure(text="30", justify=LEFT, height=1, width=10)
        self.thirty.grid(row=1, column=1, sticky=W, padx=(50, 0))

        self.thirtyFive = Button(self.myContainer4)
        self.thirtyFive.configure(text="35", justify=LEFT, height=1,
                             width=10)
        self.thirtyFive.grid(row=1, column=2, sticky=W)

        self.forty = Button(self.myContainer4)
        self.forty.configure(text="40", justify=LEFT, height=1, width=10)
        self.forty.grid(row=1, column=3, sticky=W)

        self.fortyfive = Button(self.myContainer4)
        self.fortyfive.configure(text="45", justify=LEFT, height=1, width=10)
        self.fortyfive.grid(row=1, column=4, sticky=W)

        self.fifty = Button(self.myContainer4)
        self.fifty.configure(text="50", justify=LEFT, height=1, width=10)
        self.fifty.grid(row=1, column=5, sticky=W)

        self.fiftyfive = Button(self.myContainer4)
        self.fiftyfive.configure(text="55", justify=LEFT, height=1, width=10)
        self.fiftyfive.grid(row=1, column=6, sticky=W)

        self.myContainer4.grid_forget()


        self.zero.bind("<j>", lambda event, obj="zero":
                              self.moveMenuSpotLeft(event, obj))
        self.five.bind("<j>", lambda event, obj="five":
                              self.moveMenuSpotLeft(event, obj))
        self.ten.bind("<j>", lambda event, obj="ten":
                              self.moveMenuSpotLeft(event, obj))
        self.fifteen.bind("<j>", lambda event, obj="fifteen":
                              self.moveMenuSpotLeft(event, obj))
        self.twenty.bind("<j>", lambda event, obj="twenty":
                              self.moveMenuSpotLeft(event, obj))
        self.twentyfive.bind("<j>", lambda event, obj="twentyfive":
                              self.moveMenuSpotLeft(event, obj))
        self.thirty.bind("<j>", lambda event, obj="thirty":
                              self.moveMenuSpotLeft(event, obj))
        self.thirtyFive.bind("<j>", lambda event, obj="thirtyfive":
                              self.moveMenuSpotLeft(event, obj))
        self.forty.bind("<j>", lambda event, obj="forty":
                              self.moveMenuSpotLeft(event, obj))
        self.fortyfive.bind("<j>", lambda event, obj="fortyfive":
                              self.moveMenuSpotLeft(event, obj))
        self.fifty.bind("<j>", lambda event, obj="fifty":
                              self.moveMenuSpotLeft(event, obj))
        self.fiftyfive.bind("<j>", lambda event, obj="fiftyfive":
                              self.moveMenuSpotLeft(event, obj))


        self.zero.bind("<k>", lambda event, obj="zero":
                       self.moveMenuSpotRight(event, obj))
        self.five.bind("<k>", lambda event, obj="five":
                       self.moveMenuSpotRight(event, obj))
        self.ten.bind("<k>", lambda event, obj="ten":
                      self.moveMenuSpotRight(event, obj))
        self.fifteen.bind("<k>", lambda event, obj="fifteen":
                          self.moveMenuSpotRight(event, obj))
        self.twenty.bind("<k>", lambda event, obj="twenty":
                         self.moveMenuSpotRight(event, obj))
        self.twentyfive.bind("<k>", lambda event, obj="twentyfive":
                              self.moveMenuSpotRight(event, obj))
        self.thirty.bind("<k>", lambda event, obj="thirty":
                         self.moveMenuSpotRight(event, obj))
        self.thirtyFive.bind("<k>", lambda event, obj="thirtyfive":
                             self.moveMenuSpotRight(event, obj))
        self.forty.bind("<k>", lambda event, obj="forty":
                        self.moveMenuSpotRight(event, obj))
        self.fortyfive.bind("<k>", lambda event, obj="fortyfive":
                            self.moveMenuSpotRight(event, obj))
        self.fifty.bind("<k>", lambda event, obj="fifty":
                        self.moveMenuSpotRight(event, obj))
        self.fiftyfive.bind("<k>", lambda event, obj="fiftyfive":
                            self.moveMenuSpotRight(event, obj))

        self.zero.bind("<space>", lambda event, obj="zero":
                        self.setMinSelect(event, obj))
        self.five.bind("<space>", lambda event, obj="five":
                        self.setMinSelect(event, obj))
        self.ten.bind("<space>", lambda event, obj="ten":
                        self.setMinSelect(event, obj))
        self.fifteen.bind("<space>", lambda event, obj="fifteen":
                            self.setMinSelect(event, obj))
        self.twenty.bind("<space>", lambda event, obj="twenty":
                            self.setMinSelect(event, obj))
        self.twentyfive.bind("<space>", lambda event, obj="twentyfive":
                                self.setMinSelect(event, obj))
        self.thirty.bind("<space>", lambda event, obj="thirty":
                            self.setMinSelect(event, obj))
        self.thirtyFive.bind("<space>", lambda event, obj="thirtyfive":
                                self.setMinSelect(event, obj))
        self.forty.bind("<space>", lambda event, obj="forty":
                            self.setMinSelect(event, obj))
        self.fortyfive.bind("<space>", lambda event, obj="fortyfive":
                            self.setMinSelect(event, obj))
        self.fifty.bind("<space>", lambda event, obj="fifty":
                        self.setMinSelect(event, obj))
        self.fiftyfive.bind("<space>", lambda event, obj="fiftyfive":
                            self.setMinSelect(event, obj))

    def moveMenuSpotRight(self, event, selection):
        if (selection != "fiftyfive" and selection != "exitProgram" and
                    selection != "saturday" and selection != "twelvePM"):
            print(event.widget.focus_get())
            event.widget.configure(highlightbackground='white')
            nxtwid = event.widget.tk_focusNext()
            nxtwid.configure(highlightbackground='red')
            nxtwid.focus_set()

        if (selection == "setDay"):
            setHour = Sound \
                ("wav_files/custom_sounds/Set_hour.wav")
            setHour.play()
        elif (selection == "setHour"):
            setMin = Sound \
                ("wav_files/custom_sounds/Set_min.wav")
            setMin.play()
        elif (selection == "setMin"):
            exitProgram = Sound \
                ("wav_files/custom_sounds/Exit_program.wav")
            exitProgram.play()
        elif (selection == "exitProgram"):
            if (self.myContainer2.winfo_manager() == ''):
                setDay = Sound \
                    ("wav_files/custom_sounds/Exit_program.wav")
                setDay.play()
            else:
                setSun = Sound \
                    ("wav_files/days_of_week_m/sunday_m.wav")
                setSun.play()
        elif (selection == "sunday"):
            setSun =  Sound \
                ("wav_files/days_of_week_m/monday_m.wav")
            setSun.play()
        elif (selection == "monday"):
            setMon =  Sound \
                ("wav_files/days_of_week_m/tuesday_m.wav")
            setMon.play()
        elif (selection == "tuesday"):
            setTues =  Sound \
                ("wav_files/days_of_week_m/wednesday_m.wav")
            setTues.play()
        elif (selection == "wednesday"):
            setWed =  Sound \
                ("wav_files/days_of_week_m/thursday_m.wav")
            setWed.play()
        elif (selection == "thursday"):
            setThur =  Sound \
                ("wav_files/days_of_week_m/friday_m.wav")
            setThur.play()
        elif (selection == "friday"):
            setFri = Sound \
                ("wav_files/days_of_week_m/saturday_m.wav")
            setFri.play()
        elif (selection == "saturday"):
            setSat = Sound \
                ("wav_files/days_of_week_m/saturday_m.wav")
            setSat.play()
        elif (selection == "zero"):
            setZero = Sound \
                ("wav_files/minutes_m/05_m.wav")
            setZero.play()
        elif (selection == "five"):
            setFive = Sound \
                ("wav_files/minutes_m/10_m.wav")
            setFive.play()
        elif (selection == "ten"):
            setTen = Sound \
                ("wav_files/minutes_m/15_m.wav")
            setTen.play()
        elif (selection == "fifteen"):
            setFifteen = Sound \
                ("wav_files/minutes_m/20_m.wav")
            setFifteen.play()
        elif (selection == "twenty"):
            setTwenty = Sound \
                ("wav_files/minutes_m/25_m.wav")
            setTwenty.play()
        elif (selection == "twentyfive"):
            setTwentyFive = Sound \
                ("wav_files/minutes_m/30_m.wav")
            setTwentyFive.play()
        elif (selection == "thirty"):
            setThirty = Sound \
                ("wav_files/minutes_m/35_m.wav")
            setThirty.play()
        elif (selection == "thirtyfive"):
            setThirtyFive = Sound \
                ("wav_files/minutes_m/40_m.wav")
            setThirtyFive.play()
        elif (selection == "forty"):
            setForty = Sound \
                ("wav_files/minutes_m/45_m.wav")
            setForty.play()
        elif (selection == "fortyfive"):
            setFortyFive = Sound \
                ("wav_files/minutes_m/50_m.wav")
            setFortyFive.play()
        elif (selection == "fifty"):
            setFiftyFive = Sound \
                ("wav_files/minutes_m/55_m.wav")
            setFiftyFive.play()
        elif (selection == "fiftyfive"):
            setFiftyFive = Sound \
                ("wav_files/minutes_m/55_m.wav")
            setFiftyFive.play()
        elif (selection == "oneAM"):
            setOneAM = Sound \
                ("wav_files/hours_m/2AM_m.wav")
            setOneAM.play()
        elif (selection == "onePM"):
            setOnePM = Sound \
                ("wav_files/hours_m/2PM_m.wav")
            setOnePM.play()
        elif (selection == "twoAM"):
            setTwoAM = Sound \
                ("wav_files/hours_m/3AM_m.wav")
            setTwoAM.play()
        elif (selection == "twoPM"):
            setTwoPM = Sound \
                ("wav_files/hours_m/3PM_m.wav")
            setTwoPM.play()
        elif (selection == "threeAM"):
            setThreeAM = Sound \
                ("wav_files/hours_m/4AM_m.wav")
            setThreeAM.play()
        elif (selection == "threePM"):
            setThreePM = Sound \
                ("wav_files/hours_m/4PM_m.wav")
            setThreePM.play()
        elif (selection == "fourAM"):
            setFourAM = Sound \
                ("wav_files/hours_m/5AM_m.wav")
            setFourAM.play()
        elif (selection == "fourPM"):
            setFourPM = Sound \
                ("wav_files/hours_m/5PM_m.wav")
            setFourPM.play()
        elif (selection == "fiveAM"):
            setFiveAM = Sound \
                ("wav_files/hours_m/6AM_m.wav")
            setFiveAM.play()
        elif (selection == "fivePM"):
            setFivePM = Sound \
                ("wav_files/hours_m/6PM_m.wav")
            setFivePM.play()
        elif (selection == "sixAM"):
            setSixAM = Sound \
                ("wav_files/hours_m/7AM_m.wav")
            setSixAM.play()
        elif (selection == "sixPM"):
            setSixPM = Sound \
                ("wav_files/hours_m/7PM_m.wav")
            setSixPM.play()
        elif (selection == "sevenAM"):
            setSevenAM = Sound \
                ("wav_files/hours_m/8AM_m.wav")
            setSevenAM.play()
        elif (selection == "sevenPM"):
            setSevenPM = Sound \
                ("wav_files/hours_m/8PM_m.wav")
            setSevenPM.play()
        elif (selection == "eightAM"):
            setEightAM = Sound \
                ("wav_files/hours_m/9AM_m.wav")
            setEightAM.play()
        elif (selection == "eightPM"):
            setEightPM = Sound \
                ("wav_files/hours_m/9PM_m.wav")
            setEightPM.play()
        elif (selection == "nineAM"):
            setNineAM = Sound \
                ("wav_files/hours_m/10AM_m.wav")
            setNineAM.play()
        elif (selection == "ninePM"):
            setNinePM = Sound \
                ("wav_files/hours_m/10PM_m.wav")
            setNinePM.play()
        elif (selection == "tenAM"):
            setTenAM = Sound \
                ("wav_files/hours_m/11AM_m.wav")
            setTenAM.play()
        elif (selection == "tenPM"):
            setTenPM = Sound \
                ("wav_files/hours_m/11PM_m.wav")
            setTenPM.play()
        elif (selection == "elevenAM"):
            setElevenAM = Sound \
                ("wav_files/hours_m/12AM_m.wav")
            setElevenAM.play()
        elif (selection == "elevenPM"):
            setElevePM = Sound \
                ("wav_files/hours_m/12PM_m.wav")
            setElevePM.play()
        elif (selection == "twelveAM"):
            setTwelveAM = Sound \
                ("wav_files/hours_m/1PM_m.wav")
            setTwelveAM.play()
        elif (selection == "twelvePM"):
            setTwelvePM = Sound \
                ("wav_files/hours_m/12PM_m.wav")
            setTwelvePM.play()


    def moveMenuSpotLeft(self, event, selection):
        if (selection != "sunday" and selection != "setDay" and selection !=
                "zero" and selection != "oneAM"):
            print(event.widget.focus_get())
            event.widget.configure(highlightbackground='white')
            prevwid = event.widget.tk_focusPrev()
            prevwid.configure(highlightbackground='red')
            prevwid.focus_set()

        if (selection == "setDay"):
            if (self.myContainer2.winfo_manager() == ''):
                setDay = Sound \
                    ("wav_files/custom_sounds/Set_day_menu.wav")
                setDay.play()
            else:
                setSun = Sound \
                    ("wav_files/days_of_week_m/saturday_m.wav")
                setSun.play()
        elif (selection == "setHour"):
            setMin = Sound \
                ("wav_files/custom_sounds/Set_day_menu.wav")
            setMin.play()
        elif (selection == "setMin"):
            exitProgram = Sound \
                ("wav_files/custom_sounds/Set_hour.wav")
            exitProgram.play()
        elif (selection == "exitProgram"):
            setDay = Sound \
                ("wav_files/custom_sounds/Set_min.wav")
            setDay.play()
        elif (selection == "sunday"):
            self.menuSetDay.focus_set()
            self.menuSetDay.configure(highlightbackground='red')
            self.mainMenu.configure(background='red')
            self.myContainer2.grid_forget()
            self.myParent.update()
            setDay = Sound \
                ("wav_files/custom_sounds/Main_Menu_mode.wav")
            setDay.play()
        elif (selection == "monday"):
            setMon =  Sound \
                ("wav_files/days_of_week_m/sunday_m.wav")
            setMon.play()
        elif (selection == "tuesday"):
            setTues =  Sound \
                ("wav_files/days_of_week_m/monday_m.wav")
            setTues.play()
        elif (selection == "wednesday"):
            setWed =  Sound \
                ("wav_files/days_of_week_m/tuesday_m.wav")
            setWed.play()
        elif (selection == "thursday"):
            setThur =  Sound \
                ("wav_files/days_of_week_m/wednesday_m.wav")
            setThur.play()
        elif (selection == "friday"):
            setFri = Sound \
                ("wav_files/days_of_week_m/thursday_m.wav")
            setFri.play()
        elif (selection == "saturday"):
            setsat = Sound \
                ("wav_files/days_of_week_m/friday_m.wav")
            setsat.play()
        elif (selection == "zero"):
            self.menuSetDay.focus_set()
            self.menuSetDay.configure(highlightbackground='red')
            self.mainMenu.configure(background='red')
            self.menuSetMinute.configure(highlightbackground='white')
            self.myContainer4.grid_forget()
            self.myParent.update()
            setDay = Sound \
                ("wav_files/custom_sounds/Main_Menu_mode.wav")
            setDay.play()
        elif (selection == "five"):
            setFive = Sound \
                ("wav_files/minutes_m/00_m.wav")
            setFive.play()
        elif (selection == "ten"):
            setTen = Sound \
                ("wav_files/minutes_m/05_m.wav")
            setTen.play()
        elif (selection == "fifteen"):
            setFifteen = Sound \
                ("wav_files/minutes_m/10_m.wav")
            setFifteen.play()
        elif (selection == "twenty"):
            setTwenty = Sound \
                ("wav_files/minutes_m/15_m.wav")
            setTwenty.play()
        elif (selection == "twentyfive"):
            setTwentyFive = Sound \
                ("wav_files/minutes_m/20_m.wav")
            setTwentyFive.play()
        elif (selection == "thirty"):
            setThirty = Sound \
                ("wav_files/minutes_m/25_m.wav")
            setThirty.play()
        elif (selection == "thirtyfive"):
            setThirtyFive = Sound \
                ("wav_files/minutes_m/30_m.wav")
            setThirtyFive.play()
        elif (selection == "forty"):
            setForty = Sound \
                ("wav_files/minutes_m/35_m.wav")
            setForty.play()
        elif (selection == "fortyfive"):
            setFortyFive = Sound \
                ("wav_files/minutes_m/40_m.wav")
            setFortyFive.play()
        elif (selection == "fifty"):
            setFiftyFive = Sound \
                ("wav_files/minutes_m/45_m.wav")
            setFiftyFive.play()
        elif (selection == "fiftyfive"):
            setFiftyFive = Sound \
                ("wav_files/minutes_m/50_m.wav")
            setFiftyFive.play()
        elif (selection == "oneAM"):
            self.menuSetDay.focus_set()
            self.menuSetDay.configure(highlightbackground='red')
            self.mainMenu.configure(background='red')
            self.menuSetHour.configure(highlightbackground='white')
            self.myContainer3.grid_forget()
            self.myParent.update()
            setDay = Sound \
                ("wav_files/custom_sounds/Main_Menu_mode.wav")
            setDay.play()
        elif (selection == "onePM"):
            setOnePM = Sound \
                ("wav_files/hours_m/12AM_m.wav")
            setOnePM.play()
        elif (selection == "twoAM"):
            setTwoAM = Sound \
                ("wav_files/hours_m/1AM_m.wav")
            setTwoAM.play()
        elif (selection == "twoPM"):
            setTwoPM = Sound \
                ("wav_files/hours_m/1PM_m.wav")
            setTwoPM.play()
        elif (selection == "threeAM"):
            setThreeAM = Sound \
                ("wav_files/hours_m/2AM_m.wav")
            setThreeAM.play()
        elif (selection == "threePM"):
            setThreePM = Sound \
                ("wav_files/hours_m/2PM_m.wav")
            setThreePM.play()
        elif (selection == "fourAM"):
            setFourAM = Sound \
                ("wav_files/hours_m/3AM_m.wav")
            setFourAM.play()
        elif (selection == "fourPM"):
            setFourPM = Sound \
                ("wav_files/hours_m/3PM_m.wav")
            setFourPM.play()
        elif (selection == "fiveAM"):
            setFiveAM = Sound \
                ("wav_files/hours_m/4AM_m.wav")
            setFiveAM.play()
        elif (selection == "fivePM"):
            setFivePM = Sound \
                ("wav_files/hours_m/4PM_m.wav")
            setFivePM.play()
        elif (selection == "sixAM"):
            setSixAM = Sound \
                ("wav_files/hours_m/5AM_m.wav")
            setSixAM.play()
        elif (selection == "sixPM"):
            setSixPM = Sound \
                ("wav_files/hours_m/5PM_m.wav")
            setSixPM.play()
        elif (selection == "sevenAM"):
            setSevenAM = Sound \
                ("wav_files/hours_m/6AM_m.wav")
            setSevenAM.play()
        elif (selection == "sevenPM"):
            setSevenPM = Sound \
                ("wav_files/hours_m/6PM_m.wav")
            setSevenPM.play()
        elif (selection == "eightAM"):
            setEightAM = Sound \
                ("wav_files/hours_m/7AM_m.wav")
            setEightAM.play()
        elif (selection == "eightPM"):
            setEightPM = Sound \
                ("wav_files/hours_m/7PM_m.wav")
            setEightPM.play()
        elif (selection == "nineAM"):
            setNineAM = Sound \
                ("wav_files/hours_m/8AM_m.wav")
            setNineAM.play()
        elif (selection == "ninePM"):
            setNinePM = Sound \
                ("wav_files/hours_m/8PM_m.wav")
            setNinePM.play()
        elif (selection == "tenAM"):
            setTenAM = Sound \
                ("wav_files/hours_m/9AM_m.wav")
            setTenAM.play()
        elif (selection == "tenPM"):
            setTenPM = Sound \
                ("wav_files/hours_m/9PM_m.wav")
            setTenPM.play()
        elif (selection == "elevenAM"):
            setElevenAM = Sound \
                ("wav_files/hours_m/10AM_m.wav")
            setElevenAM.play()
        elif (selection == "elevenPM"):
            setElevePM = Sound \
                ("wav_files/hours_m/10PM_m.wav")
            setElevePM.play()
        elif (selection == "twelveAM"):
            setTwelveAM = Sound \
                ("wav_files/hours_m/11AM_m.wav")
            setTwelveAM.play()
        elif (selection == "twelvePM"):
            setTwelvePM = Sound \
                ("wav_files/hours_m/11PM_m.wav")
            setTwelvePM.play()


    def setDayClick(self, event):

        self.myContainer2.grid()
        self.mainMenu.configure(background='white')
        self.menuSetDay.configure(highlightbackground='white')
        self.myParent.update()

        setDayMode = Sound \
            ("wav_files/custom_sounds/Set_day_mode1.wav")
        setDayMode.play_to_end()
        playSetDayIntro()

        currentDay = currentClock.getCurrentDay()
        if currentDay == "Sunday":
            self.sunday.configure(highlightbackground='red')
            self.sunday.focus_set()
        elif currentDay == "Monday":
            self.monday.configure(highlightbackground='red')
            self.monday.focus_set()
        elif currentDay == "Tuesday":
            self.tuesday.configure(highlightbackground='red')
            self.tuesday.focus_set()
        elif currentDay == "Wednesday":
            self.wednesday.configure(highlightbackground='red')
            self.wednesday.focus_set()
        elif currentDay == "Thursday":
            self.thursday.configure(highlightbackground='red')
            self.thursday.focus_set()
        elif currentDay == "Friday":
            self.friday.configure(highlightbackground='red')
            self.friday.focus_set()
        elif currentDay == "Saturday":
            self.saturday.configure(highlightbackground='red')
            self.saturday.focus_set()

    def setDaySelect(self, event, selection):
        if (selection == "sunday"):
            currentClock.setCurrentDay("Sunday")
        elif (selection == "monday"):
            currentClock.setCurrentDay("Monday")
        elif (selection == "tuesday"):
            currentClock.setCurrentDay("Tuesday")
        elif (selection == "wednesday"):
            currentClock.setCurrentDay("Wednesday")
        elif (selection == "thursday"):
            currentClock.setCurrentDay("Thursday")
        elif (selection == "friday"):
            currentClock.setCurrentDay("Friday")
        elif (selection == "saturday"):
            currentClock.setCurrentDay("Saturday")

        self.myContainer2.grid_forget()
        self.mainMenu.configure(background='red')
        self.menuSetDay.configure(highlightbackground='red')
        self.menuSetDay.focus_set()
        self.currentTimeLabel.destroy()
        if (currentClock.getCurrentMinute() < 10):
            self.minClockDisplace = "0" + str(currentClock.getCurrentMinute())
        else:
            self.minClockDisplace = str(currentClock.getCurrentMinute())
        self.currentTimeLabel = Label(self.myParent, text="Current Set Time: " +
                                     str(currentClock.getCurrentDay()) + " " +
                                     str(currentClock.getCurrentHour()) + ":" +
                                     self.minClockDisplace + " " +
                                     str(currentClock.getCurrentAMPM()),
                                      font="Times 30 bold")
        self.currentTimeLabel.place(relx=0.0, rely=0.5, anchor="w")
        self.myParent.update()

        daySelected = Sound \
            ("wav_files/days_of_week_m/" + selection + "_m.wav")
        daySelected.play_to_end()
        selected = Sound \
            ("wav_files/custom_sounds/selected.wav")
        selected.play_to_end()

        mainMenuMode = Sound \
            ("wav_files/custom_sounds/Main_Menu_Mode.wav")
        mainMenuMode.play()

    def setHourClick(self, event):
        self.menuSetHour.configure(highlightbackground='white')
        self.myContainer3.grid()
        self.mainMenu.configure(background='white')
        self.myParent.update()

        setHourMode = Sound \
            ("wav_files/custom_sounds/Set_hour_mode.wav")
        setHourMode.play_to_end()
        playSetHourIntro()

        currentHour = currentClock.getCurrentHour()
        currentAMPM = currentClock.getCurrentAMPM()
        if currentHour == 0:
            if currentAMPM == "AM":
                self.zeroAM.configure(highlightbackground='red')
                self.zeroAM.focus_set()
            else:
                self.zeroPM.configure(highlightbackground='red')
                self.zeroPM.focus_set()
        elif currentHour == 1:
            if currentAMPM == "AM":
                self.oneAM.configure(highlightbackground='red')
                self.oneAM.focus_set()
            else:
                self.onePM.configure(highlightbackground='red')
                self.onePM.focus_set()
        elif currentHour == 2:
            if currentAMPM == "AM":
                self.twoAM.configure(highlightbackground='red')
                self.twoAM.focus_set()
            else:
                self.twoPM.configure(highlightbackground='red')
                self.twoPM.focus_set()
        elif currentHour == 3:
            if currentAMPM == "AM":
                self.threeAM.configure(highlightbackground='red')
                self.threeAM.focus_set()
            else:
                self.threePM.configure(highlightbackground='red')
                self.threePM.focus_set()
        elif currentHour == 4:
            if currentAMPM == "AM":
                self.fourAM.configure(highlightbackground='red')
                self.fourAM.focus_set()
            else:
                self.fourPM.configure(highlightbackground='red')
                self.fourPM.focus_set()
        elif currentHour == 5:
            if currentAMPM == "AM":
                self.sixAM.configure(highlightbackground='red')
                self.sixAM.focus_set()
            else:
                self.sixPM.configure(highlightbackground='red')
                self.sixPM.focus_set()
        elif currentHour == 6:
            if currentAMPM == "AM":
                self.sixAM.configure(highlightbackground='red')
                self.sixAM.focus_set()
            else:
                self.sixPM.configure(highlightbackground='red')
                self.sixPM.focus_set()
        elif currentHour == 7:
            if currentAMPM == "AM":
                self.sevenAM.configure(highlightbackground='red')
                self.sevenAM.focus_set()
            else:
                self.sevenPM.configure(highlightbackground='red')
                self.sevenPM.focus_set()
        elif currentHour == 8:
            if currentAMPM == "AM":
                self.eightAM.configure(highlightbackground='red')
                self.eightAM.focus_set()
            else:
                self.eightPM.configure(highlightbackground='red')
                self.eightPM.focus_set()
        elif currentHour == 9:
            if currentAMPM == "AM":
                self.nineAM.configure(highlightbackground='red')
                self.nineAM.focus_set()
            else:
                self.ninePM.configure(highlightbackground='red')
                self.ninePM.focus_set()
        elif currentHour == 10:
            if currentAMPM == "AM":
                self.tenAM.configure(highlightbackground='red')
                self.tenAM.focus_set()
            else:
                self.tenPM.configure(highlightbackground='red')
                self.tenPM.focus_set()
        elif currentHour == 11:
            if currentAMPM == "AM":
                self.elevenAM.configure(highlightbackground='red')
                self.elevenAM.focus_set()
            else:
                self.elevenPM.configure(highlightbackground='red')
                self.elevenPM.focus_set()
        elif currentHour == 12:
            if currentAMPM == "AM":
                self.twelveAM.configure(highlightbackground='red')
                self.twelveAM.focus_set()
            else:
                self.twelvePM.configure(highlightbackground='red')
                self.twelvePM.focus_set()

    def setHourSelect(self, event, selection):
        if (selection == "oneAM"):
            currentClock.setCurrentHour(1)
            currentClock.setCurrentAMPM("AM")
        elif (selection == "onePM"):
            currentClock.setCurrentHour(1)
            currentClock.setCurrentAMPM("PM")
        elif (selection == "twoAM"):
            currentClock.setCurrentHour(2)
            currentClock.setCurrentAMPM("AM")
        elif (selection == "twoPM"):
            currentClock.setCurrentHour(2)
            currentClock.setCurrentAMPM("PM")
        elif (selection == "threeAM"):
            currentClock.setCurrentHour(3)
            currentClock.setCurrentAMPM("AM")
        elif (selection == "threePM"):
            currentClock.setCurrentHour(3)
            currentClock.setCurrentAMPM("PM")
        elif (selection == "fourAM"):
            currentClock.setCurrentHour(4)
            currentClock.setCurrentAMPM("AM")
        elif (selection == "fourPM"):
            currentClock.setCurrentHour(4)
            currentClock.setCurrentAMPM("PM")
        elif (selection == "fiveAM"):
            currentClock.setCurrentHour(5)
            currentClock.setCurrentAMPM("AM")
        elif (selection == "fivePM"):
            currentClock.setCurrentHour(5)
            currentClock.setCurrentAMPM("PM")
        elif (selection == "sixAM"):
            currentClock.setCurrentHour(6)
            currentClock.setCurrentAMPM("AM")
        elif (selection == "sixPM"):
            currentClock.setCurrentHour(6)
            currentClock.setCurrentAMPM("PM")
        elif (selection == "sevenAM"):
            currentClock.setCurrentHour(7)
            currentClock.setCurrentAMPM("AM")
        elif (selection == "sevenPM"):
            currentClock.setCurrentHour(7)
            currentClock.setCurrentAMPM("PM")
        elif (selection == "eightAM"):
            currentClock.setCurrentHour(8)
            currentClock.setCurrentAMPM("AM")
        elif (selection == "eightPM"):
            currentClock.setCurrentHour(8)
            currentClock.setCurrentAMPM("PM")
        elif (selection == "nineAM"):
            currentClock.setCurrentHour(9)
            currentClock.setCurrentAMPM("AM")
        elif (selection == "ninePM"):
            currentClock.setCurrentHour(9)
            currentClock.setCurrentAMPM("PM")
        elif (selection == "tenAM"):
            currentClock.setCurrentHour(10)
            currentClock.setCurrentAMPM("AM")
        elif (selection == "tenPM"):
            currentClock.setCurrentHour(10)
            currentClock.setCurrentAMPM("PM")
        elif (selection == "elevenAM"):
            currentClock.setCurrentHour(11)
            currentClock.setCurrentAMPM("AM")
        elif (selection == "elevenPM"):
            currentClock.setCurrentHour(11)
            currentClock.setCurrentAMPM("PM")
        elif (selection == "twelveAM"):
            currentClock.setCurrentHour(12)
            currentClock.setCurrentAMPM("AM")
        elif (selection == "twelvePM"):
            currentClock.setCurrentHour(12)
            currentClock.setCurrentAMPM("PM")


        self.myContainer3.grid_forget()
        self.mainMenu.configure(background='red')
        self.menuSetDay.configure(highlightbackground='red')
        self.menuSetDay.focus_set()
        self.menuSetHour.configure(highlightbackground='white')
        self.currentTimeLabel.destroy()
        if (currentClock.getCurrentMinute() < 10):
            self.minClockDisplace = "0" + str(currentClock.getCurrentMinute())
        else:
            self.minClockDisplace = str(currentClock.getCurrentMinute())
        self.currentTimeLabel = Label(self.myParent, text="Current Set Time: " +
                              str(currentClock.getCurrentDay()) + " " +
                              str(currentClock.getCurrentHour()) + ":" +
                              self.minClockDisplace + " " +
                              str(currentClock.getCurrentAMPM()),
                              font="Times 30 bold")
        self.currentTimeLabel.place(relx=0.0, rely=0.5, anchor="w")
        self.myParent.update()

        x = str(currentClock.getCurrentHour())
        y = currentClock.getCurrentAMPM()

        hourSOund = Sound \
            ("wav_files/hours_m/" + x + y + "_m.wav")
        hourSOund.play_to_end()

        selected = Sound \
            ("wav_files/custom_sounds/selected.wav")
        selected.play_to_end()

        mainMenuMode = Sound \
            ("wav_files/custom_sounds/Main_Menu_Mode.wav")
        mainMenuMode.play()

    def setMinuteClick(self, event):

        self.menuSetMinute.configure(highlightbackground='white')
        self.myContainer4.grid()
        self.mainMenu.configure(background='white')
        self.myParent.update()

        setMinMode = Sound \
            ("wav_files/custom_sounds/Set_min_mode.wav")
        setMinMode.play_to_end()
        playSetMinuteIntro()

        currentMin = currentClock.getCurrentMinute()
        if currentMin == 0:
            self.zero.configure(highlightbackground='red')
            self.zero.focus_set()
        elif currentMin == 5:
            self.five.configure(highlightbackground='red')
            self.five.focus_set()
        elif currentMin == 10:
            self.ten.configure(highlightbackground='red')
            self.ten.focus_set()
        elif currentMin == 15:
            self.fifteen.configure(highlightbackground='red')
            self.fifteen.focus_set()
        elif currentMin == 20:
            self.twenty.configure(highlightbackground='red')
            self.twenty.focus_set()
        elif currentMin == 25:
            self.twentyfive.configure(highlightbackground='red')
            self.twentyfive.focus_set()
        elif currentMin == 30:
            self.thirty.configure(highlightbackground='red')
            self.thirty.focus_set()
        elif currentMin == 35:
            self.thirtyFive.configure(highlightbackground='red')
            self.thirtyFive.focus_set()
        elif currentMin == 40:
            self.forty.configure(highlightbackground='red')
            self.forty.focus_set()
        elif currentMin == 45:
            self.fortyfive.configure(highlightbackground='red')
            self.fortyfive.focus_set()
        elif currentMin == 50:
            self.fifty.configure(highlightbackground='red')
            self.fifty.focus_set()
        elif currentMin == 55:
            self.fiftyfive.configure(highlightbackground='red')
            self.fiftyfive.focus_set()

    def setMinSelect(self,event, selection):
        if (selection == "zero"):
            currentClock.setCurrentMin(0)
        elif (selection == "five"):
            currentClock.setCurrentMin(5)
        elif (selection == "ten"):
            currentClock.setCurrentMin(10)
        elif (selection == "fifteen"):
            currentClock.setCurrentMin(15)
        elif (selection == "twenty"):
            currentClock.setCurrentMin(20)
        elif (selection == "twentyfive"):
            currentClock.setCurrentMin(25)
        elif (selection == "thirty"):
            currentClock.setCurrentMin(30)
        elif (selection == "thirtyfive"):
            currentClock.setCurrentMin(35)
        elif (selection == "forty"):
            currentClock.setCurrentMin(40)
        elif (selection == "fortyfive"):
            currentClock.setCurrentMin(45)
        elif (selection == "fifty"):
            currentClock.setCurrentMin(50)
        elif (selection == "fiftyfive"):
            currentClock.setCurrentMin(55)

        self.myContainer4.grid_forget()
        self.mainMenu.configure(background='red')
        self.menuSetDay.configure(highlightbackground='red')
        self.menuSetDay.focus_set()
        self.menuSetMinute.configure(highlightbackground='white')
        self.currentTimeLabel.destroy()
        if (currentClock.getCurrentMinute() < 10):
            self.minClockDisplace = "0" + str(currentClock.getCurrentMinute())
        else:
            self.minClockDisplace = str(currentClock.getCurrentMinute())
        self.currentTimeLabel = Label(self.myParent, text="Current Set Time: " +
                                      str(currentClock.getCurrentDay()) + " " +
                                      str(currentClock.getCurrentHour()) + ":" +
                                      self.minClockDisplace + " "
                                      + str(currentClock.getCurrentAMPM()),
                                      font="Times 30 bold")
        self.currentTimeLabel.place(relx=0.0, rely=0.5, anchor="w")
        self.myParent.update()

        x = str(currentClock.getCurrentMinute())
        if (currentClock.getCurrentMinute() < 10):
            minSound = Sound \
                ("wav_files/minutes_m/0" + x + "_m.wav")
            minSound.play_to_end()
        else:
            minSound = Sound \
                ("wav_files/minutes_m/" + x + "_m.wav")
            minSound.play_to_end()

        selected = Sound \
            ("wav_files/custom_sounds/selected.wav")
        selected.play_to_end()

        mainMenuMode = Sound \
            ("wav_files/custom_sounds/Main_Menu_Mode.wav")
        mainMenuMode.play()


    def exitProgramPress (self, event):
        SAVED_DAY = currentClock.getCurrentDay()
        SAVED_HOUR = currentClock.getCurrentHour()
        SAVED_MINUTE = currentClock.getCurrentMinute()
        SAVED_AMPM = currentClock.getCurrentAMPM()

        EXITING_PROGRAM.play_to_end()
        with open("SavedTime.txt", "w") as text_file:
            text_file.write(str.format(SAVED_DAY) + " " +
                            str.format(str(SAVED_HOUR)) + " " +
                            str.format(str(SAVED_MINUTE)) + " " +
                            str.format(str(SAVED_AMPM)))
        quit()


def playSetDayIntro():
    '''
    Plays the announcement that the user is now in the 'Set Day' state and then
    pronounces what the current day is set to by calling playCurrentDay() from
    the day class.
    '''
    SET_DAY_INTRO = Sound \
        ("wav_files/custom_sounds/Current_day_set_to.wav")
    SET_DAY_INTRO.play_to_end()
    currentClock.playCurrentDay()
    return
def playSetHourIntro():
    '''
    Plays the announcement that the user is now in the 'Set Hour' state and then
    pronounces what the current hour is set to by calling playCurrentHour() from
    the day class.
    '''
    SET_HOUR_INTRO = Sound \
        ("wav_files/custom_sounds/set_hour_intro.wav")
    SET_HOUR_INTRO.play_to_end()
    currentClock.playCurrentHour()
    return

def playSetMinuteIntro():
    '''
    Plays the announcement that the user is now in the 'Set Minute' state and
    then pronounces what the current minute is set to by calling
    playCurrentMinute() from the day class.
    '''
    SET_MINUTE_INTRO = Sound \
        ("wav_files/custom_sounds/set_minute_intro.wav")
    SET_MINUTE_INTRO.play_to_end()
    currentClock.playCurrentMinute()
    return


def on_closing():
    if messagebox.askokcancel("Quit", "Do you want to save changes?") == True:
        SAVED_DAY = currentClock.getCurrentDay()
        SAVED_HOUR = currentClock.getCurrentHour()
        SAVED_MINUTE = currentClock.getCurrentMinute()
        SAVED_AMPM = currentClock.getCurrentAMPM()

        EXITING_PROGRAM.play_to_end()
        with open("SavedTime.txt", "w") as text_file:
            text_file.write(str.format(SAVED_DAY) + " " +
                            str.format(str(SAVED_HOUR)) + " " +
                            str.format(str(SAVED_MINUTE)) + " " +
                            str.format(str(SAVED_AMPM)))
        quit()

    else:
        print("Not saved.")
        quit()


try:
    f = open(SAVED_TIME)
    line_time = f.readline()
    lstTime = []
    lstTime = line_time.split()
    currentClock = day.CurrentDay(lstTime[0], int(lstTime[1]), int(lstTime[2]),
                                  lstTime[3])
except IOError:
    currentClock = day.CurrentDay(DEFAULT_DAY, DEFAULT_HOUR, DEFAULT_MINUTE,
                                  DEFAULT_AMPM)

SAVED_DAY = currentClock.getCurrentDay()
SAVED_HOUR = currentClock.getCurrentHour()
SAVED_MINUTE = currentClock.getCurrentMinute()
SAVED_AMPM = currentClock.getCurrentAMPM()
root = Tk()
root.wm_title("Set Clock")
root.protocol("WM_DELETE_WINDOW", on_closing)
myapp = MyApp(root)
root.mainloop()