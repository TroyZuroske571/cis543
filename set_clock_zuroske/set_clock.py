################################################################################
# set_clock2_zuroske_initial.py
# T. Zuroske - Sept 2016
#
# A program that allows a user to set a clock using only auditory commands.
#
# Some sample code used from sample_menu.py implemented by A. Hornof - Sept 2016
################################################################################

__author__ = 'zuroske'

# Package imports
import readchar
import day

# Local imports
from sound import Sound

# Variables
DEFAULT_DAY = "Sunday"
DEFAULT_HOUR = 12
DEFAULT_MINUTE = 00
DEFAULT_AMPM = "AM"
SAVED_DAY = None
SAVED_HOUR = None
SAVED_MINUTE = None
SAVED_AMPM = None
QUIT_MENU_KEY = ' '
FORWARD = 'j'
BACKWARD = 'k'
SELECT = ' '
SET_MINUTE = 'l'
RETURN_TO_MENU = 'l'
HELP = ';'
DAYS = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
        "SATURDAY"]
LAST_DAY = 6
FIRST_DAY = 0
DAY_NUMBER = 0
SAVED_DAY_NUM = 0
LAST_HOUR = 12
FIRST_HOUR = 1
CURRENT_HOUR = 12
CURRENT_AMPM = "AM"
IS_AM = True
CURRENT_MIN = 0
FIRST_MIN = 0
LAST_MIN = 59
FOR_HELP = Sound ("wav_files/custom_sounds/For_help_press.wav")
HELP_MAIN = Sound ("wav_files/custom_sounds/user_interaction.wav")
NO_CHANGES_SAVED = Sound ("wav_files/custom_sounds/No_changes_saved.wav")
CURRENT_MODE_MAIN = Sound ("wav_files/custom_sounds/Current_mode_main.wav")
QUIT_PROGRAM = Sound \
            ("wav_files/menus_modes_navigation_m/Press_again_to_quit_m.wav")
EXITING_PROGRAM = Sound \
                        ("wav_files/menus_modes_navigation_m/"
                         "Exiting_program_m.wav")
SAVED_TIME = "SavedTime.txt"

def welcomeUser():
    '''
    Plays the welcome introduction audio file to the user
    '''
    WELCOME = Sound \
        ("wav_files/custom_sounds/welcome.wav")
    WELCOME.play_to_end()
    return

def playCurrentDayTime():
    '''
    Plays the announcement "The current time is..." and then calls the function
    playCurrentDayAndTime() to play the actual time from the day class.
    '''
    CURRENT_CLOCK_INTRO = Sound \
        ("wav_files/custom_sounds/Clock_currently_set_to.wav")
    CURRENT_CLOCK_INTRO.play_to_end()
    currentClock.playCurrentDayAndTime()
    return

def playSetDayIntro():
    '''
    Plays the announcement that the user is now in the 'Set Day' state and then
    pronounces what the current day is set to by calling playCurrentDay() from
    the day class.
    '''
    SET_DAY_INTRO = Sound \
        ("wav_files/custom_sounds/Current_day_set_to.wav")
    SET_DAY_INTRO.play_to_end()
    currentClock.playCurrentDay()
    FOR_HELP.play_to_end()
    return

s

def playSetMinuteIntro():
    '''
    Plays the announcement that the user is now in the 'Set Minute' state and
    then pronounces what the current minute is set to by calling
    playCurrentMinute() from the day class.
    '''
    SET_MINUTE_INTRO = Sound \
        ("wav_files/custom_sounds/set_minute_intro.wav")
    SET_MINUTE_INTRO.play_to_end()
    currentClock.playCurrentMinute()
    FOR_HELP.play_to_end()
    return


def setDay():
    '''
    Allows the user to set the day by reading in keyboard strokes. The user
    moves forward through the hours by pressing 'j', and backwards by pressing
    'k'. Also gives the user options to go back by pressing 'l' and get help
    by replaying the instructions by pressing ';'.
    '''
    HELP_DAY = Sound("wav_files/custom_sounds/Help_set_day.wav")
    SET_DAY_MODE = Sound("wav_files/custom_sounds/Set_day_mode.wav")

    playSetDayIntro()
    while True:
        global SAVED_DAY_NUM
        global SAVED_DAY
        global DAY_NUMBER
        # Enter Set day state
        # Get the first keystroke.
        userInput = readchar.readchar()
        # Respond to the user's input.
        if userInput == FORWARD:
            if DAY_NUMBER == LAST_DAY:
                DAY_NUMBER = 0
            else:
                DAY_NUMBER += 1
            daySound = Sound \
                ("wav_files/days_of_week_m/" + DAYS[
                    DAY_NUMBER].lower() + "_m.wav")
            daySound.play_to_end()

        elif userInput == BACKWARD:
            if DAY_NUMBER == FIRST_DAY:
                DAY_NUMBER = 6
            else:
                DAY_NUMBER -= 1
            daySound = Sound \
                ("wav_files/days_of_week_m/" + DAYS[
                    DAY_NUMBER].lower() + "_m.wav")
            daySound.play_to_end()

        elif userInput == SELECT:
            selectDay = Sound \
                ("wav_files/menus_modes_navigation_m/you_selected_m.wav")
            selectDay.play_to_end()
            day = Sound \
                ("wav_files/days_of_week_m/" + DAYS[
                    DAY_NUMBER].lower() + "_m.wav")
            day.play_to_end()
            currentClock.setCurrentDay(DAYS[DAY_NUMBER])
            SAVED_DAY_NUM = DAY_NUMBER
            SAVED_DAY = DAYS[DAY_NUMBER]
            return
        elif userInput == HELP:
            SET_DAY_MODE.play_to_end()
            HELP_DAY.play_to_end()
            playSetDayIntro()
            DAY_NUMBER = SAVED_DAY_NUM
        elif userInput == RETURN_TO_MENU:
            NO_CHANGES_SAVED.play_to_end()
            DAY_NUMBER = SAVED_DAY_NUM
            return
    return

def setHour():
    '''
    Allows the user to set the hour by reading in keyboard strokes. The user
    moves forward through the hours by pressing 'j', and backwards by pressing
    'k'. Also gives the user options to go back by pressing 'l' and get help
    by replaying the instructions by pressing ';'.
    '''

    HELP_HOUR = Sound("wav_files/custom_sounds/Help_set_hour.wav")
    playSetHourIntro()

    while True:
        global CURRENT_HOUR
        global CURRENT_AMPM
        global IS_AM
        global SAVED_HOUR
        global SAVED_AMPM
        # Get the first keystroke.
        userInput = readchar.readchar()
        if userInput == FORWARD:
            if CURRENT_HOUR == LAST_HOUR:
                CURRENT_HOUR = 1
            elif CURRENT_HOUR == LAST_HOUR - 1:
                if IS_AM:
                    CURRENT_AMPM = "PM"
                    IS_AM = False
                    CURRENT_HOUR += 1
                else:
                    CURRENT_AMPM = "AM"
                    IS_AM = True
                    CURRENT_HOUR += 1
            else:
                CURRENT_HOUR += 1
            hourSound = Sound \
                ("wav_files/hours_m/" + str(CURRENT_HOUR) +  CURRENT_AMPM +
                 "_m.wav")
            hourSound.play_to_end()

        elif userInput == BACKWARD:
            if CURRENT_HOUR == FIRST_HOUR:
                CURRENT_HOUR = 12
            elif CURRENT_HOUR == LAST_HOUR:
                if IS_AM:
                    CURRENT_AMPM = "PM"
                    IS_AM = False
                    CURRENT_HOUR -= 1
                else:
                    CURRENT_AMPM = "AM"
                    IS_AM = True
                    CURRENT_HOUR -= 1
            else:
                CURRENT_HOUR -= 1
            hourSound = Sound \
                ("wav_files/hours_m/" + str(CURRENT_HOUR) + CURRENT_AMPM +
                 "_m.wav")
            hourSound.play_to_end()

        elif userInput == SELECT:
            selectHour = Sound \
                ("wav_files/menus_modes_navigation_m/you_selected_m.wav")
            selectHour.play_to_end()
            hourSound = Sound \
                ("wav_files/hours_m/" + str(CURRENT_HOUR) + CURRENT_AMPM +
                 "_m.wav")
            hourSound.play_to_end()
            currentClock.setCurrentHour(CURRENT_HOUR)
            currentClock.setCurrentAMPM(CURRENT_AMPM)
            SAVED_HOUR = CURRENT_HOUR
            SAVED_AMPM = CURRENT_AMPM
            return
        elif userInput == HELP:
            HELP_HOUR.play_to_end()
            playSetHourIntro()
            CURRENT_HOUR = SAVED_HOUR
            CURRENT_AMPM = SAVED_AMPM
        elif userInput == RETURN_TO_MENU:
            NO_CHANGES_SAVED.play_to_end()
            CURRENT_HOUR = SAVED_HOUR
            CURRENT_AMPM = SAVED_AMPM
            return
    return


def setMinute():
    '''
    Allows the user to set the minute by reading in keyboard strokes. The user
    moves forward through the hours by pressing 'j', and backwards by pressing
    'k'. Also gives the user options to go back by pressing 'l' and get help
    by replaying the instructions by pressing ';'.
    '''
    HELP_MINUTE = Sound("wav_files/custom_sounds/Help_set_minute.wav")


    while True:
        global CURRENT_MIN
        global SAVED_MINUTE
        # Get the first keystroke.
        userInput = readchar.readchar()
        if userInput == FORWARD:
            if CURRENT_MIN == LAST_MIN:
                CURRENT_MIN = 0
            else:
                CURRENT_MIN += 1
            if CURRENT_MIN < 10:
                minSound = Sound \
                    ("wav_files/minutes_m/0" + str(CURRENT_MIN) + "_m.wav")
                minSound.play_to_end()
            else:
                minSound = Sound \
                    ("wav_files/minutes_m/" + str(CURRENT_MIN) + "_m.wav")
                minSound.play_to_end()

        elif userInput == BACKWARD:
            if CURRENT_MIN == FIRST_MIN:
                CURRENT_MIN = 59
            else:
                CURRENT_MIN -= 1
            if CURRENT_MIN < 10:
                minSound = Sound \
                    ("wav_files/minutes_m/0" + str(CURRENT_MIN) + "_m.wav")
                minSound.play_to_end()
            else:
                minSound = Sound \
                    ("wav_files/minutes_m/" + str(CURRENT_MIN) + "_m.wav")
                minSound.play_to_end()

        elif userInput == SELECT:
            selectMin = Sound \
                ("wav_files/menus_modes_navigation_m/you_selected_m.wav")
            selectMin.play_to_end()
            if CURRENT_MIN < 10:
                zeroSound = Sound \
                    ("wav_files/minutes_m/00_m.wav")
                zeroSound.play_to_end()
                minSound = Sound \
                    ("wav_files/minutes_m/0" + str(CURRENT_MIN) + "_m.wav")
                minSound.play_to_end()
            else:
                minSound = Sound \
                    ("wav_files/minutes_m/" + str(CURRENT_MIN) + "_m.wav")
                minSound.play_to_end()
            currentClock.setCurrentMin(CURRENT_MIN)
            SAVED_MINUTE = CURRENT_MIN
            return
        elif userInput == HELP:
            HELP_MINUTE.play_to_end()
            playSetMinuteIntro()
            CURRENT_MIN = SAVED_MINUTE
        elif userInput == RETURN_TO_MENU:
            NO_CHANGES_SAVED.play_to_end()
            CURRENT_MIN = SAVED_MINUTE
            return
    return


# Main program execution.

# A minimal amount of text to display to guide the user.
MINIMAL_HELP_STRING = "Press '" + QUIT_MENU_KEY + "' to quit."

################################################################################

# Provide a minimal indication that the program has started.
print(MINIMAL_HELP_STRING)

'''
Open and read file if exits if not set to default and instantiate a day object
called currentClock that will be used throughout the states to set the day,
hours, and minutes.
'''

try:
    f = open(SAVED_TIME)
    line_time = f.readline()
    lstTime = []
    lstTime = line_time.split()
    currentClock = day.CurrentDay(lstTime[0], int(lstTime[1]), int(lstTime[2]),
                                  lstTime[3])
except IOError:
    currentClock = day.CurrentDay(DEFAULT_DAY, DEFAULT_HOUR, DEFAULT_MINUTE,
                                  DEFAULT_AMPM)


SAVED_DAY = currentClock.getCurrentDay()
SAVED_HOUR = currentClock.getCurrentHour()
SAVED_MINUTE = currentClock.getCurrentMinute()
SAVED_AMPM = currentClock.getCurrentAMPM()
bHelp = False

while True:
    day = currentClock.getCurrentDay()
    playCurrentDayTime()
    if not bHelp:
        FOR_HELP.play_to_end()
    userInput = readchar.readchar()
    if userInput == FORWARD:
        setDay()
        bHelp = False
    elif userInput == BACKWARD:
        setHour()
        bHelp = False
    elif userInput == SET_MINUTE:
        setMinute()
        bHelp = False
    elif userInput == HELP:
        CURRENT_MODE_MAIN.play_to_end()
        HELP_MAIN.play_to_end()
        bHelp = True
    elif userInput == QUIT_MENU_KEY:
        QUIT_PROGRAM.play_to_end()
        while True:
            userInput = readchar.readchar()
            if userInput == QUIT_MENU_KEY:
                EXITING_PROGRAM.play_to_end()
                with open("SavedTime.txt", "w") as text_file:
                    text_file.write(str.format(SAVED_DAY) + " " +
                                    str.format(str(SAVED_HOUR)) + " " +
                                    str.format(str(SAVED_MINUTE)) + " " +
                                    str.format(str(SAVED_AMPM)))
                quit()
            else:
                bHelp = False
                break
