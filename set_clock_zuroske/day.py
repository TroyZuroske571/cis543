################################################################################
# day.py
# T. Zuroske - Sept 2016
#
# A class that keeps track of day and time with auditory functions.
################################################################################

__author__ = 'zuroske'


# Package imports
from sound import Sound


class CurrentDay:
    def __init__(self, dayOfWeek, hour, minute, ampm):
        '''
        The constructor for the day class.

        :param dayOfWeek: the current day
        :param hour: the current hour
        :param minute: the current minute
        :param ampm: whether it is am or pm
        '''
        self.day = dayOfWeek
        self.hour = hour
        self.minute = minute
        self.ampm = ampm

    def setCurrentDay(self, dayOfWeek):
        '''
        Sets the current day
        :param dayOfWeek:
        :return: nothing
        '''
        self.day = dayOfWeek

    def setCurrentHour(self, hour):
        '''
        Sets the current hour
        :param hour: the current hour
        :return: nothing
        '''
        self.hour = hour

    def setCurrentMin(self, minute):
        '''
        Sets the current minute
        :param minute: the
        :return: nothing
        '''
        self.minute = minute

    def setCurrentAMPM(self, ampm):
        '''
        Sets whether it is am or pm
        :param ampm: The day set to am or pm
        :return: nothing
        '''
        self.ampm = ampm

    def getCurrentDay(self):
        '''
        Gets the current day
        :return: the current day as a string
        '''
        return self.day

    def getCurrentHour(self):
        '''
        Gets the current hour
        :return: the current hour as an integer
        '''
        return self.hour

    def getCurrentMinute(self):
        '''
        Gets the current minute.
        :return: The current minute.
        '''
        return self.minute

    def getCurrentAMPM(self):
        '''
        Gets whether the day is am or pm
        :return: The string am or pm.
        '''
        return self.ampm

    def playCurrentDay(self):
        '''
        Plays the current day to the user.
        :return: Nothing.
        '''
        DAY = Sound \
            ("wav_files/days_of_week_m/" + self.day.lower() + "_m.wav")
        DAY.play_to_end()

    def playCurrentHour(self):
        '''
        Plays the current hour to the user.
        :return: Nothing.
        '''
        HOUR = Sound \
            ("wav_files/hours_m/" + str(self.hour) + self.ampm.upper() +
             "_m.wav")
        HOUR.play_to_end()

    def playCurrentMinute(self):
        '''
        Plays the current minute.
        :return: Nothing.
        '''
        if self.minute < 10:
            zeroSound = Sound \
                ("wav_files/minutes_m/00_m.wav")
            zeroSound.play_to_end()
            MIN = Sound \
                ("wav_files/minutes_m/0" + str(self.minute) + "_m.wav")
            MIN.play_to_end()
        else:
            MIN = Sound \
                ("wav_files/minutes_m/" + str(self.minute) + "_m.wav")
            MIN.play_to_end()

    def playCurrentDayAndTime(self):
        '''
        Plays the current day and time.
        :return: Nothing.
        '''
        DAY = Sound \
            ("wav_files/days_of_week_m/" + self.day.lower() +"_m.wav")
        DAY.play_to_end()
        if self.hour < 10:
            HOUR = Sound \
                ("wav_files/minutes_m/0" + str(self.hour) +"_m.wav")
            HOUR.play_to_end()
        else:
            HOUR = Sound \
                ("wav_files/minutes_m/" + str(self.hour) + "_m.wav")
            HOUR.play_to_end()
        if self.minute == 00:
            MIN = Sound \
                ("wav_files/menus_modes_navigation_m/o_clock_m.wav")
            MIN.play_to_end()
        elif self.minute < 10:
            ZERO = Sound \
                ("wav_files/minutes_m/00_m.wav")
            ZERO.play_to_end()
            MIN = Sound \
                ("wav_files/minutes_m/0" + str(self.minute) + "_m.wav")
            MIN.play_to_end()
        else:
            MIN = Sound \
                ("wav_files/minutes_m/" + str(self.minute) +"_m.wav")
            MIN.play_to_end()
        AMPM = Sound \
                ("wav_files/hours_m/" + self.ampm + "_m.wav")
        AMPM.play_to_end()




